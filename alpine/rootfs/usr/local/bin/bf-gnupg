#!/bin/sh

set -euo pipefail

#======================================================================================================================
# Executable usage.
#======================================================================================================================
usage () {
    echo "Usage: `basename ${0}` [-a|-k|-f|-g]" 2>&1
    echo
    echo "Set change action:"
    echo "    -a    asc key url."
    echo "    -k    keyserver url"
    echo "    -f    file"
    echo "    -g    GPG_KEYS string"
    exit 2
}

while getopts ":ha:k:f:g:" option ; do
    case ${option} in
        h) usage ;;
        a) ASC_URL=$OPTARG;;
        k) KEYSERVER=$OPTARG;;
        f) FILE=$OPTARG;;
        g) GPG_KEYS=$OPTARG;;
        \?) bf-notok "Invalid option: -${OPTARG}" "bf-gnupg" && usage ;;
    esac
done

shift $(($OPTIND -1))

[[ -z "${ASC_URL-}" ]] && bf-error "You must provide the asc key url." "bf-gnupg"
[[ -z "${KEYSERVER-}" ]] && bf-error "You must provide keyserver url." "bf-gnupg"
[[ -z "${FILE-}" ]] && bf-error "You must provide file." "bf-gnupg"
[[ -z "${GPG_KEYS-}" ]] && bf-error "You must provide GPG_KEYS." "bf-gnupg"

if [ -f "$FILE" ]; then
    bf-echo "$FILE exists." "bf-gnupg"
else
    bf-error "$FILE does not exist." "bf-gnupg"
fi

bf-apk-add --virtual .fetch-deps gnupg

s6-mkdir -p /tmp/gnupg
cd /tmp/gnupg

asc_file="asc_file.asc"

curl -fsSL -o $asc_file "$ASC_URL"
export GNUPGHOME="$(mktemp -d)"
for key in $GPG_KEYS; do
  gpg --batch --keyserver $KEYSERVER --recv-keys "$key";
done;

gpg --batch --verify $asc_file $FILE
gpgconf --kill all

rm -rf "$GNUPGHOME"

bf-apk-del .fetch-deps

bf-clear
bf-done