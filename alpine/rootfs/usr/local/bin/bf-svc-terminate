#!/usr/bin/with-contenv sh

set -euo pipefail


#======================================================================================================================
# Check environment variable.
#======================================================================================================================

[[ -z "${S6_SERVICES-}" ]] && bf-error "S6_SERVICES environment variable is not set." "bf-svc-terminate"


#======================================================================================================================
# Check if it has already been called (multiple services may try to bring it down).
#======================================================================================================================

if [ "${BF_TERMINATING-}" = "1" ] ; then
    bf-debug "Services already being terminated." "bf-svc-terminate"
    exit 0
fi

bf-env "BF_TERMINATING" "1"


#======================================================================================================================
# Terminate all running services (will stop / restart the container depending on restart policy).
#======================================================================================================================

bf-notok "Terminating all services." "bf-svc-terminate"
s6-svscanctl -t ${S6_SERVICES}
