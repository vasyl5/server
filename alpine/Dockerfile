FROM alpine:3.14 AS base

FROM base AS builder

RUN apk add --no-cache alpine-sdk sudo \
    && adduser -s /bin/sh -D -G abuild abuild \
    && echo "%abuild ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER abuild
RUN abuild-keygen -a -i -n -q

FROM builder AS mozjpeg

WORKDIR /tmp/mozjpeg
COPY --chown=abuild:abuild apk/mozjpeg/APKBUILD ./
RUN sudo chown abuild:abuild ./ \
    && abuild checksum \
    && abuild deps \
    && abuild -r


FROM base as alpine_base

ARG HOME=/var/www
ARG LOG_STDOUT

ENV BF_DEBUG=1
ENV	BF_SRC=/usr/src
ENV	TERM=xterm-256color
ENV	PAGER=more
ENV	TZ="Europe/Amsterdam"
ENV	PS1="$(whoami)@$(hostname):$(pwd)$ "

ENV	WWW_USER=www-data
ENV	WWW_USER_GROUP=www-data
ENV	HOME=${HOME:-/var/www}
ENV	LOG_PREFIX=/var/log
ENV	LOG_STDOUT=${LOG_STDOUT:-true}
ENV CACHE_PREFIX=/var/cache
ENV LIB_DIR_PREFIX=/var/lib
ENV SERVER_ROOT_PREFIX=$HOME/html
ENV	PUID=${PUID:-1000}
ENV PGID=${PGID:-82}
ENV S6_SERVICES=/var/run/s6/services

# Customizing s6 behaviour https://github.com/just-containers/s6-overlay#customizing-s6-behaviour

ENV	S6_BEHAVIOUR_IF_STAGE2_FAILS=1
ENV	S6_KILL_FINISH_MAXTIME=18000
ENV S6_FIX_ATTRS_HIDDEN=1
ENV SOCKLOG_TIMESTAMP_FORMAT=T

COPY --from=mozjpeg /home/abuild/packages/tmp/**/*.apk /apk_build/mozjpeg/

RUN set -x \
    && adduser -u $PUID -D -S -h $HOME -s /bin/bash -G $WWW_USER_GROUP -g $WWW_USER $WWW_USER \
    && apk add --no-cache --update --allow-untrusted /apk_build/mozjpeg/*.apk \
    && rm -rf /apk_build

FROM alpine_base AS release

COPY rootfs /

# https://github.com/just-containers/socklog-overlay

RUN set -x \
    && chmod +x /usr/local/bin/bf-* \
    && bf-apk-add \
    	s6-overlay \
	    bash \
	    bash-completion \
    	socklog \
    	esh@main-edge \
    	coreutils \
    	curl \
    	tar \
    	xz \
    	git \
    && bf-tz $TZ \
    && bf-clear \
    # s6-fdholderd active by default \
    # https://github.com/just-containers/s6-overlay/issues/192
    && s6-rmrf /etc/s6/services/s6-fdholderd/down

HEALTHCHECK --interval=30s --timeout=10s --start-period=5s --retries=5 CMD [ "healthcheck" ]

ENTRYPOINT ["/init"]