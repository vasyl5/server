group "default" {
  targets = [
    "alpine_release",
    "php_release",
    "nginx"
  ]
}
variable "TAG" {
  default = ""
}

variable "IS_PUSH" {
  default = false
}

variable "DOCKER_REGISTRY" {
  default = "registry.gitlab.com/vasyl5/server"
}

# Functions section
function "get_tags" {
  params = [image_tag]
  result = [
    "${DOCKER_REGISTRY}/${image_tag}",
    notequal("",TAG) ? "${DOCKER_REGISTRY}/${image_tag}:${TAG}":"",
    IS_PUSH ? "" : get_builder_tag("${image_tag}"),
  ]
}

function "get_builder_tag" {
  params = [image_tag]
  result = "docker-builder:${image_tag}"
}

function "get_builder_tags" {
  params = [image_tag]
  result = [
    get_builder_tag("${image_tag}")
  ]
}

function "get_cache_from" {
  params = [image_tag]
  result = "${DOCKER_REGISTRY}/${image_tag}:latest"
}

# Base config
target "buildkit" {
  args = {
    BUILDKIT_INLINE_CACHE = 1
  }
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    "linux/arm64"
  ]
}

target "base-alpine-cache" {
  inherits = ["buildkit"]
  context = "./alpine"
  target = "base"
  cache-from = [
    get_builder_tag("alpine-base")
  ]
  tags = get_builder_tags("alpine-base")
}

target "docker-build-base" {
  inherits = ["base-alpine-cache"]
  target = "builder"
  cache-from = [
    get_builder_tag("builder")
  ]
  tags = get_builder_tags("builder")
}

target "docker-build-mozjpeg" {
  inherits = ["docker-build-base"]
  target = "mozjpeg"
  cache-from = [
    get_builder_tag("mozjpeg")
  ]
  tags = get_builder_tags("mozjpeg")
}

target "alpine_base" {
  inherits = ["base-alpine-cache"]
  target = "alpine_base"
  cache-from = [
    get_builder_tag("alpine_base"),
    get_builder_tag("mozjpeg")
  ]
  tags = get_builder_tags("alpine_base")
}

target "alpine_release" {
  inherits = ["alpine_base"]
  target = "release"
  cache-from = [
    get_builder_tag("alpine_base"),
    get_builder_tag("alpine-s6")
  ]
  tags = get_tags("alpine-s6")
}

target "php_install" {
  inherits = ["buildkit"]
  context = "./php-fpm"
  target = "php_install"
  pull = true
  cache-from = [
    get_cache_from("alpine-s6"),
    get_builder_tag("php_install"),
  ]
  tags = get_builder_tags("php_install")
}

target "php_release" {
  inherits = ["php_install"]
  target = "release"
  pull = false
  cache-from = [
    get_builder_tag("php_install"),
    get_builder_tag("php-fpm"),
  ]
  tags = get_tags("php-fpm")
}

target "nginx" {
  inherits = ["buildkit"]
  context = "./nginx"
  cache-from = [
    get_builder_tag("php-fpm"),
    get_builder_tag("nginx"),
  ]
  tags = get_tags("nginx")
}