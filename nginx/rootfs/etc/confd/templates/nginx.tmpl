include                                 /etc/nginx/modules/*.conf;

user                                    www-data;
worker_processes                        auto;
worker_rlimit_nofile                    65535;
timer_resolution                        100ms;
pcre_jit                                on;
thread_pool                             default threads=32 max_queue=65536;

events {
	worker_connections                  65535;
	multi_accept                        on;
	use                                 epoll;
}

http {

	include                             conf.d/init.conf;

    geo                                 $rate_limit {include geo.d/ratelimit.conf;}
    map                                 $rate_limit $rate_limit_key {include map.d/access/*.map;}
    map                                 $http_user_agent $no_logs {include map.d/logs/ua.map;}

    limit_req_zone                      $rate_limit_key zone=req_zone:10m rate=200r/s;

    upstream php-fpm-upstream           {include upstream.d/php-fpm.conf;}

    include                             conf.d/log-format.conf;

    access_log                          /dev/stdout main_ext;
    error_log                           /dev/stderr error;

    map                                 $http_x_forwarded_proto $proxy_x_forwarded_proto {include map.d/header/proto.map;}
    map                                 $http_x_forwarded_port $proxy_x_forwarded_port {include map.d/header/port.map;}
    map                                 $http_upgrade $proxy_connection {include map.d/header/upgrade.map;}
    map                                 $scheme $proxy_x_forwarded_ssl {include map.d/header/scheme.map;}
    map                                 $host:$server_port$request_uri $noindex {include map.d/header/robot.map;}
    map                                 $request_method $skip_fetch {include map.d/srcache/*.map;}

    map                                 $sent_http_content_type $expires {include map.d/cache/expires.map;}
    map                                 $http_cookie $php_session_cookie {include map.d/cache/phpsession.map;}

    map                                 $request_uri $redirect_uri {include map.d/redirects/*.map;}
    map                                 $request_uri $no_cache {include map.d/nocache/nocache.map;}
    map                                 $http_cookie $no_cache {include map.d/nocache/cookie.map;}

    map                                 $http_user_agent $crawler_pre {include map.d/referrer/crawler.map;}
    map                                 $http_user_agent $bot_pre {include map.d/referrer/bot.map;}
    map                                 $args $prerender {default $bot_pre; "~(^|&)_escaped_fragment_=" 1;}

    fastcgi_cache_path                  {{getenv "NGINX_CACHE_PREFIX"}}/fastcgi keys_zone=fastcgicache:10m levels=1:2 inactive=30m max_size=64m;
    fastcgi_cache_key                   $scheme$request_method$host$request_uri$php_session_cookie;

    proxy_cache_path                    {{getenv "NGINX_CACHE_PREFIX"}}/proxy keys_zone=proxycache:10m levels=1:2 inactive=30m max_size=64m;
    proxy_cache_key                     $scheme$request_method$http_host$request_uri;

    map                                 $request_method $purge_method {include map.d/purge/*.map;}

    geo                                 $purge_allowed {include geo.d/purge.conf;}
    geo                                 $whitelist {include geo.d/whitelist.conf;}
    map                                 $whitelist $limit_access {include map.d/access/*.map;}
    expires                             $expires;

    index                               index.php index.html default.html;

    include                             conf.d/brotli.conf;
    include                             conf.d/gzip.conf;
    include                             conf.d/proxy.conf;
    include                             conf.d/botblocker-nginx-settings.conf;
    include                             conf.d/globalblacklist.conf;
    include                             sites-available/*.conf;
}
