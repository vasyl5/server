#!/usr/bin/with-contenv bash

set -euo pipefail

mkdir -p \
	$NGINX_LIB_DIR/tmp \
	$NGINX_LOG_PREFIX \
	$NGINX_CACHE_PREFIX/fastcgi \
	$NGINX_CACHE_PREFIX/proxy \
	$SERVER_ROOT_PREFIX/$SERVER_DOCROOT

cat <<EOF >"${NGINX_CONF_PREFIX}/upstream.d/php-fpm.conf"
zone                            php-fpm 64k;
hash                            \$scheme\$request_uri;
keepalive                       64;
server                          unix:${PHP_LISTEN_PREFIX}/www.sock max_fails=3 fail_timeout=30s;

EOF

cat <<EOF >"${NGINX_CONF_PREFIX}/sites-available/default.conf"
server                              {
  server_tokens                   off;
  server_name                     ${NGINX_SERVER_NAME:-_};
  listen                          *:${NGINX_LISTEN_PORT:-8080} default_server;
  listen                          [::]:${NGINX_LISTEN_PORT:-8080} default_server reuseport;
  root                            ${SERVER_ROOT_PREFIX}${SERVER_DOCROOT};
  index                           index.php index.html;

  set                             \$naxsi_flag_enable ${NGINX_ENABLE_NAXSI:-1};
  set                             \$no_cache 1;
  http2_push_preload              on;

  #access_log                      /var/run/s6/nginx-website-access-log-fifo main_ext;
  error_log                       /var/run/s6/nginx-website-error-log-fifo error;

  include                         bots.d/blockbots.conf;
  include                         bots.d/ddos.conf;
  include                         header.d/httpd.conf;
  include                         location.d/*.conf;
}
EOF

cat <<EOF >"${NGINX_CONF_PREFIX}/nginx.conf"
include                                 ${NGINX_CONF_PREFIX}/modules/*.conf;

user                                    $WWW_USER;
worker_processes                        auto;
worker_rlimit_nofile                    65535;
timer_resolution                        100ms;
pcre_jit                                on;
thread_pool                             default threads=32 max_queue=65536;

events {
	worker_connections                  65535;
	multi_accept                        on;
	use                                 epoll;
}

http {

	include                             conf.d/init.conf;

  geo                                 \$rate_limit {include geo.d/ratelimit.conf;}
  map                                 \$rate_limit \$rate_limit_key {include map.d/access/*.map;}
  map                                 \$http_user_agent \$no_logs {include map.d/logs/ua.map;}

  limit_req_zone                      \$rate_limit_key zone=req_zone:10m rate=200r/s;

  upstream php-fpm-upstream           {include upstream.d/php-fpm.conf;}

  include                             conf.d/log-format.conf;

  # to boost I/O on HDD we can disable access logs
  access_log                          off;
  error_log                           /dev/stderr error;

  map                                 \$http_x_forwarded_proto \$proxy_x_forwarded_proto {include map.d/header/proto.map;}
  map                                 \$http_x_forwarded_port \$proxy_x_forwarded_port {include map.d/header/port.map;}
  map                                 \$http_upgrade \$proxy_connection {include map.d/header/upgrade.map;}
  map                                 \$scheme \$proxy_x_forwarded_ssl {include map.d/header/scheme.map;}
  map                                 \$host:\$server_port\$request_uri \$noindex {include map.d/header/robot.map;}
  map                                 \$request_method \$skip_fetch {include map.d/srcache/*.map;}

  map                                 \$sent_http_content_type \$expires {include map.d/cache/expires.map;}
  map                                 \$http_cookie \$php_session_cookie {include map.d/cache/phpsession.map;}

  map                                 \$request_uri \$redirect_uri {include map.d/redirects/*.map;}
  map                                 \$request_uri \$no_cache {include map.d/nocache/nocache.map;}
  map                                 \$http_cookie \$no_cache {include map.d/nocache/cookie.map;}

  map                                 \$http_user_agent \$crawler_pre {include map.d/referrer/crawler.map;}
  map                                 \$http_user_agent \$bot_pre {include map.d/referrer/bot.map;}
  map                                 \$args \$prerender {default \$bot_pre; "~(^|&)_escaped_fragment_=" 1;}

  fastcgi_cache_path                  ${NGINX_CACHE_PREFIX}/fastcgi keys_zone=fastcgicache:10m levels=1:2 inactive=30m max_size=64m;
  fastcgi_cache_key                   \$scheme\$request_method\$host\$request_uri\$php_session_cookie;

  proxy_cache_path                    ${NGINX_CACHE_PREFIX}/proxy keys_zone=proxycache:10m levels=1:2 inactive=30m max_size=64m;
  proxy_cache_key                     \$scheme\$request_method\$http_host\$request_uri;

  map                                 \$request_method \$purge_method {include map.d/purge/*.map;}

  geo                                 \$purge_allowed {include geo.d/purge.conf;}
  geo                                 \$whitelist {include geo.d/whitelist.conf;}
  map                                 \$whitelist \$limit_access {include map.d/access/*.map;}
  expires                             \$expires;

  index                               index.php index.html;

  include                             conf.d/brotli.conf;
  include                             conf.d/gzip.conf;
  include                             conf.d/proxy.conf;
  include                             conf.d/botblocker-nginx-settings.conf;
  include                             conf.d/globalblacklist.conf;
  include                             sites-available/*.conf;
}

EOF


bf-chown $NGINX_LIB_DIR
bf-chown $NGINX_CONF_PREFIX
bf-chown $NGINX_CACHE_PREFIX
#bf-chown $SERVER_ROOT_PREFIX
rm -rf $HOME/localhost